package fr.yahoo.phbrandao.tresor;

import fr.yahoo.phbrandao.tresor.entite.Carte;
import junit.framework.TestCase;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Principaux scénarios de test
 */
public class CarteTest extends TestCase {
    String repFichiersCarte = "src/test/resources/carte/";
    String repFichiersResultat = "target/fichiers_sortie_test/";

    /**
     * Méthode commune à tous les tests: écrit le répertoire de sortie
     * @throws IOException Erreur lors de la lecture du fichier d'entrée
     */
    public void setUp() throws Exception {
        super.setUp();
        Files.createDirectories(Paths.get(repFichiersResultat));
    }

    /**
     * Méthode commune à tous les tests : calcule le checksum d'un fichier pour comparer les résultats attendus et produits
     */
    private String getChecksum(String fichier) {
        try (InputStream is = Files.newInputStream(Paths.get(fichier))) {
            return DigestUtils.md5Hex(is);
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }
        return null;
    }

    /**
     * Méthode commune à une partie des tests : joue un tour
     * @throws IOException Erreur lors de la lecture du fichier d'entrée
     */
    private void jouerUnTour(String fichierEntree, String fichierResultat) throws IOException {
        Carte carte = new Carte(fichierEntree);
        carte.afficherCarteDansLaConsole();
        carte.jouerUnTour(0);
        carte.ecrireFichierResultat(fichierResultat);
    }

    /**
     * Méthode commune à une partie des tests : joue tous les tours
     * @throws IOException Erreur lors de la lecture du fichier d'entrée
     */
    private void jouerTousLesTours(String fichierEntree, String fichierResultat) throws IOException {
        Carte carte = new Carte(fichierEntree);
        carte.afficherCarteDansLaConsole();

        int numTour = 1;
        boolean resteDesTours = true;
        do {
            resteDesTours = carte.jouerUnTour(numTour++);
        } while (resteDesTours);

        carte.ecrireFichierResultat(fichierResultat);
    }

    /**
     * Compare les checksum de deux fichiers
     * @throws IOException Erreur lors de la lecture du fichier d'entrée
     */
    private void comparerFichiers(String fichierResultat, String fichierAttendu) throws IOException {
        String checksumResultat = getChecksum(fichierResultat);
        String checksumAttendu = getChecksum(fichierAttendu);
        assertEquals(checksumResultat, checksumAttendu);
        Files.deleteIfExists(Paths.get(fichierResultat));
    }

    /**
     * Scénario 1 : l'aventurier tente de sortir de la carte
     * Résultat attendu : il n'avance pas
     * @throws IOException Erreur lors de la lecture du fichier d'entrée
     */
    public void testScenario1() throws IOException {
        String racineFichiers = "1_sort_de_la_carte";
        String fichierEntree = repFichiersCarte + racineFichiers + ".txt";
        String fichierAttendu = repFichiersCarte + racineFichiers + "_attendu.txt";
        String fichierResultat = repFichiersResultat + racineFichiers + "_resultat.txt";

        System.out.println("\nScénario 1 : l'aventurier tente de sortir de la carte");
        jouerUnTour(fichierEntree, fichierResultat);

        comparerFichiers(fichierResultat, fichierAttendu);
    }

    /**
     * Scénario 2 : l'aventurier avance vers un case libre (plaine)
     * Résultat attendu : il avance
     * @throws IOException Erreur lors de la lecture du fichier d'entrée
     */
    public void testScenario2() throws IOException {
        String racineFichiers = "2_avance_vers_plaine";
        String fichierEntree = repFichiersCarte + racineFichiers + ".txt";
        String fichierAttendu = repFichiersCarte + racineFichiers + "_attendu.txt";
        String fichierResultat = repFichiersResultat + racineFichiers + "_resultat.txt";

        System.out.println("\nScénario 2 : l'aventurier avance vers un case libre (plaine)");
        jouerUnTour(fichierEntree, fichierResultat);

        comparerFichiers(fichierResultat, fichierAttendu);
    }

    /**
     * Scénario 3 : l'aventurier est bloqué par une montagne
     * Résultat attendu : il ne bouge pas
     * @throws IOException Erreur lors de la lecture du fichier d'entrée
     */
    public void testScenario3() throws IOException {
        String racineFichiers = "3_bloque_par_montagne";
        String fichierEntree = repFichiersCarte + racineFichiers + ".txt";
        String fichierAttendu = repFichiersCarte + racineFichiers + "_attendu.txt";
        String fichierResultat = repFichiersResultat + racineFichiers + "_resultat.txt";

        System.out.println("\nScénario 3 : l'aventurier est bloqué par une montagne");
        jouerUnTour(fichierEntree, fichierResultat);

        comparerFichiers(fichierResultat, fichierAttendu);
    }

    /**
     * Scénario 4 : l'aventurier ramasse un trésor
     * Résultat attendu : il avance vers le trésor et le récolte
     * @throws IOException Erreur lors de la lecture du fichier d'entrée
     */
    public void testScenario4() throws IOException {
        String racineFichiers = "4_ramasse_tresor";
        String fichierEntree = repFichiersCarte + racineFichiers + ".txt";
        String fichierAttendu = repFichiersCarte + racineFichiers + "_attendu.txt";
        String fichierResultat = repFichiersResultat + racineFichiers + "_resultat.txt";

        System.out.println("\nScénario 4 : l'aventurier ramasse un trésor");
        jouerUnTour(fichierEntree, fichierResultat);

        comparerFichiers(fichierResultat, fichierAttendu);
    }

    /**
     * Scénario 5 : 3 aventuriers avancent en file indienne l'un derrière l'autre
     * Résultat attendu : il avancent tous les trois
     * @throws IOException Erreur lors de la lecture du fichier d'entrée
     */
    public void testScenario5() throws IOException {
        String racineFichiers = "5_trois_aventuriers_avancent_en_file_indienne";
        String fichierEntree = repFichiersCarte + racineFichiers + ".txt";
        String fichierAttendu = repFichiersCarte + racineFichiers + "_attendu.txt";
        String fichierResultat = repFichiersResultat + racineFichiers + "_resultat.txt";

        System.out.println("\nScénario 5 : 3 aventuriers avancent en file indienne l'un derrière l'autre");
        jouerUnTour(fichierEntree, fichierResultat);

        comparerFichiers(fichierResultat, fichierAttendu);
    }

    /**
     * Scénario 6 : deux aventuriers sont face à face et avancent
     * Résultat attendu : il avancent tous les deux, permutant ainsi leur position
     * @throws IOException Erreur lors de la lecture du fichier d'entrée
     */
    public void testScenario6() throws IOException {
        String racineFichiers = "6_deux_aventuriers_sont_bloques_en_boucle";
        String fichierEntree = repFichiersCarte + racineFichiers + ".txt";
        String fichierAttendu = repFichiersCarte + racineFichiers + "_attendu.txt";
        String fichierResultat = repFichiersResultat + racineFichiers + "_resultat.txt";

        System.out.println("\nScénario 6 : deux aventuriers sont face à face et avancent");
        jouerUnTour(fichierEntree, fichierResultat);

        comparerFichiers(fichierResultat, fichierAttendu);
    }

    /**
     * Scénario 10 : Scénario complet
     * Résultat attendu : le fichier résulat est confirme à l'attendu
     * @throws IOException Erreur lors de la lecture du fichier d'entrée
     */
    public void testScenarioComplet() throws IOException {
        String racineFichiers = "10_scenario_complet";
        String fichierEntree = repFichiersCarte + racineFichiers + ".txt";
        String fichierAttendu = repFichiersCarte + racineFichiers + "_attendu.txt";
        String fichierResultat = repFichiersResultat + racineFichiers + "_resultat.txt";

        System.out.println("\nScénario 10 : Scénario complet");
        jouerTousLesTours(fichierEntree, fichierResultat);

        comparerFichiers(fichierResultat, fichierAttendu);
    }
}