package fr.yahoo.phbrandao.tresor;

import fr.yahoo.phbrandao.tresor.entite.Carte;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.IOException;

/**
 * Tests de base de cas d'erreur
 */
public class CasDErreurTest extends TestCase
{
    /**
     * Le fichier d'entrée n'existe pas
     */
    public void testNomDeFichierIncorrect() {
        boolean exceptionLevee = false;

        String fichierTest = "fichier_inexistant.txt";
        try {
            Carte carte = new Carte(fichierTest);
        } catch (IOException e) {
            exceptionLevee = true;
        }
        assertTrue( exceptionLevee );
    }
}
