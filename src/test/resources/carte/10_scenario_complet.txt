C - 5 - 4

M - 2 - 0
M - 3 - 0
M - 1 - 3

T - 2 - 2 - 2
T - 3 - 3 - 4

# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Séquence de mouvement
A - Indiana - 1 - 0 - N - DA
A - Jones - 2 - 1 - S - AGA
A - Albert - 3 - 2 - O - AGADA
A - Claire - 0 - 2 - S - A
A - Lara - 0 - 3 - O - A
A - Bill - 0 - 1 - E - A
