package fr.yahoo.phbrandao.tresor.enumerations;

/**
 * Orientations possibles pour un aventurier : nord, sud, est, ouest
 */
public enum Orientation {
    /** Nord */
    NORD("N"),
    /** Sud */
    SUD("S"),
    /** Est */
    EST("E"),
    /** Ouest */
    OUEST("O");

    private final String label;

    Orientation(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
