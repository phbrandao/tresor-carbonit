package fr.yahoo.phbrandao.tresor.enumerations;

/**
 * Mouvements possibles pour un aventurier : avancer, tourner à droite et tourner à gauche
 */
public enum Mouvement {
    /**
     * Avancer
     */
    AVANCER,
    /** Tourner à gauche */
    TOURNER_A_GAUCHE,
    /**
     * Avancer
     */
    TOURNER_A_DROITE
}
