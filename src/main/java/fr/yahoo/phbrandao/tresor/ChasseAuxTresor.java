package fr.yahoo.phbrandao.tresor;

import fr.yahoo.phbrandao.tresor.entite.Carte;

import java.io.IOException;

/**
 * Chasse au trésor au pérou
 * @author Philippe Brandao (phbrandao@yahoo.fr)
 */
public class ChasseAuxTresor
{
    /**
     * Constructeur par défaut, pas utilisé
     */
    public ChasseAuxTresor() {
    }

    /**
     * Méthode principale
     *
     * @param args
     *      1- Le chemin complet du fichier d'entrée
     *      2- Le chemin complet du fichier résultat
     * @throws
     *      IOException en cas d'erreur dans la lecture du fichier d'entrée
     */
    public static void main( String[] args ) throws IOException, RuntimeException {
        String cheminFichierEntree;
        String cheminFichierSortie;

        System.out.println("\nLa chasse au trésor au pérou commence !");

        // Si aucun fichier d'entrée n'est donné en paramètre, on fourni un fichier de test
        if (args.length == 0) {
            System.out.println("Attention: aucun fichier d'entrée et résulat n'ont été passés en paramètre. Un scenario de test va être joué");
            cheminFichierEntree = "src/test/resources/carte/10_scenario_complet.txt";
            cheminFichierSortie = "resultat.txt";
        } else if (args.length != 2) {
            throw new RuntimeException("Veuillez passer deux paramètres: le fihcier d'entrée et le fichier de sortie. Si aucun paramètre n'est passé, des valeurs par défaut seront proposées.");
        } else {
            cheminFichierEntree = args[0];
            System.out.printf("Fichier soumis en entrée: %s", cheminFichierEntree);
            cheminFichierSortie = args[1];
            System.out.printf("Fichier résultat: %s", cheminFichierSortie);
        }

        // On initialise la carte à partir du fichier et on l'affiche
        Carte carte = new Carte(cheminFichierEntree);
        System.out.println("\nCarte au début de la chasse au trésor :");
        carte.afficherCarteDansLaConsole();

        jouerTousLesTours(carte);

        carte.ecrireFichierResultat(cheminFichierSortie);
    }

    /**
     * Joue la partie complète. Toutes les données nécessaires sont dans la carte
     * @param carte
     *      La carte contenant les montagnes, trésors et aventuriers, ainsi que leurs mouvements
     * @throws RuntimeException Erreur applicative
     */
    public static void jouerTousLesTours(Carte carte) throws RuntimeException {
        // On joue les tours un par un tant qu'il en reste
        int numTour = 1;
        boolean resteDesTours;
        do {
            resteDesTours = carte.jouerUnTour(numTour++);
        } while (resteDesTours);
    }
}
