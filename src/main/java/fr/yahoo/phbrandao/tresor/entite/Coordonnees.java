package fr.yahoo.phbrandao.tresor.entite;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Coordonnées d'un élément sur la cart, qui peut être une montagne, un trésor ou un aventurier
 * Une montagne n'a aucun autre attribut que ses coordonnées sur la carte. Il n'y a donc pas de claase Montagne
 */
@Data
@AllArgsConstructor
public sealed class Coordonnees implements Comparable<Coordonnees> permits Tresor, Aventurier {
    private Integer x;
    private Integer y;

    /**
     * Constructeur par défaut, pas utilisé
     */
    public Coordonnees() {
    }

    @Override
    public int compareTo(Coordonnees otherCoordonnees) {
        int compareY = Integer.compare(getY(), otherCoordonnees.getY());

        if (compareY != 0)
            return compareY;
        else
            return Integer.compare(getX(), otherCoordonnees.getX());
    }
}
