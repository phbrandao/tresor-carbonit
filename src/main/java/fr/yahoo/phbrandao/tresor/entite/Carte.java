package fr.yahoo.phbrandao.tresor.entite;

import fr.yahoo.phbrandao.tresor.enumerations.Mouvement;
import fr.yahoo.phbrandao.tresor.enumerations.Orientation;

import java.io.*;
import java.util.*;

/**
 * La classe carte contient tous les éléments qui la compose (montagnes, trésors et aventuriers), ainsi que toute la
 * logique applicative
 */
public class Carte {
    private int tailleX;
    private int tailleY;
    private List<Coordonnees> montagnesEtTresors = new ArrayList<Coordonnees>();
    private List<Aventurier> aventuriers = new ArrayList<Aventurier>();

    /**
     * Constructeur de l'objet Carte. Parse le fichier d'entrée et remplis la carte
     * @param fichierEntree
     *      Le fichier d'entrée au format texte contenant tous les éléments de la carte : montagnes, trésors et aventuriers
     * @throws
     *      IOException : erreur lors de la lecture du fichier d'entrée ou écriture dans le fichier de sortie
     *      RuntimeException : erreur applicative
     */
    public Carte(String fichierEntree) throws IOException, RuntimeException {
        try {
            File fichier = new File(fichierEntree);    // Créer l'objet File à partir du chemin complet du fichier donné en paramètre
            FileReader fr = new FileReader(fichier);
            BufferedReader br = new BufferedReader(fr);
            String ligneBrut;
            int ordreAventurier = 0;

            System.out.println("\nDétails du scénario :");
            while ((ligneBrut = br.readLine()) != null) {
                if (!ligneBrut.startsWith("#") && !ligneBrut.isEmpty()) {
                    System.out.println(ligneBrut);
                    String[] partiesLigne = ligneBrut.split("-");
                    List<String> elementsLigne = new ArrayList<String>();
                    for (String s : partiesLigne) {
                        elementsLigne.add(s.trim());
                    }
                    if (elementsLigne.size() < 3)
                        throw new RuntimeException("Une ligne doit contenir au moins 3 champs");

                    int coordX;
                    int coordY;
                    switch (elementsLigne.get(0)) {
                        case "C":
                            tailleX = Integer.parseInt(elementsLigne.get(1));
                            tailleY = Integer.parseInt(elementsLigne.get(2));
                            break;
                        case "M":
                            coordX = Integer.parseInt(elementsLigne.get(1));
                            coordY = Integer.parseInt(elementsLigne.get(2));
                            lePointEstDansLaCarte(coordX, coordY, true);
                            montagnesEtTresors.add(new Coordonnees(coordX,coordY));
                            break;
                        case "T":
                            coordX = Integer.parseInt(elementsLigne.get(1));
                            coordY = Integer.parseInt(elementsLigne.get(2));
                            lePointEstDansLaCarte(coordX, coordY, true);
                            montagnesEtTresors.add(new Tresor(coordX, coordY, Integer.parseInt(elementsLigne.get(3))));
                            break;
                        case "A":
                            coordX = Integer.parseInt(elementsLigne.get(2));
                            coordY = Integer.parseInt(elementsLigne.get(3));
                            lePointEstDansLaCarte(coordX, coordY, true);
                            char orientationBrut = elementsLigne.get(4).charAt(0);
                            Orientation orientation;
                            switch (orientationBrut) {
                                case 'N':
                                    orientation = Orientation.NORD;
                                    break;
                                case 'S':
                                    orientation = Orientation.SUD;
                                    break;
                                case 'E':
                                    orientation = Orientation.EST;
                                    break;
                                case 'O':
                                    orientation = Orientation.OUEST;
                                    break;
                                default:
                                    throw new RuntimeException("Orientation " + orientationBrut + " incorrect à la ligne : " + ligneBrut);
                            }

                            String listeMouvementsBrut = elementsLigne.get(5);
                            Deque<Mouvement> listeMouvements = new ArrayDeque<Mouvement>();
                            for (int i = 0; i < listeMouvementsBrut.length(); i++) {
                                switch (listeMouvementsBrut.charAt(i)) {
                                    case 'A':
                                        listeMouvements.offerLast(Mouvement.AVANCER);
                                        break;
                                    case 'D':
                                        listeMouvements.offerLast(Mouvement.TOURNER_A_DROITE);
                                        break;
                                    case 'G':
                                        listeMouvements.offerLast(Mouvement.TOURNER_A_GAUCHE);
                                        break;
                                    default:
                                        throw new RuntimeException("Mouvement " + listeMouvementsBrut.charAt(i) + " incorrect à la ligne : " + ligneBrut);
                                }
                            }
                            aventuriers.add(new Aventurier(coordX, coordY, ordreAventurier++, elementsLigne.get(1), orientation, listeMouvements));
                            break;
                        default:
                            throw new RuntimeException("L'élement " + elementsLigne.get(0) + " est inconnu");
                    }
                }
            }
            fr.close();
            Collections.sort(montagnesEtTresors);
            Collections.sort(aventuriers);
        } catch (IOException e) {
            throw new IOException("Erreur lors de la lecture du fichier d'entrée");
        }
    }

    /**
     * Vérifie que les coordonnées passée en paramètre sont dans les limites la carte
     * @param coordX
     *      abcisse de la coordonnées
     * @param coordX
     *      ordonnée de la coordonnées
     * @param declencheException
     *      Si vrai, déclencheune exception de type RuntimeException si le point est hors de la carte
     * @return vrai si le point est dans la carte, sauf s'il est hors des limites
     * @throws RuntimeException : erreur applicative
     */
    private boolean lePointEstDansLaCarte(int coordX, int coordY, boolean declencheException) throws RuntimeException {
        if (tailleX == 0 || tailleY == 0)
            throw new RuntimeException("La taille de la carte n'a pas bien été initialisée");
        else if (coordX < 0 || coordX > tailleX - 1 || coordY < 0 || coordY > tailleY - 1) {
            if (declencheException)
                throw new RuntimeException("Erreur lors de l'interprétation du fichier d'entrée : le point est en dehors de la carte");
            else
                return true;
        }
        else if (Collections.binarySearch(montagnesEtTresors, new Coordonnees(coordX, coordY), Comparator.naturalOrder()) >= 0
            || Collections.binarySearch(aventuriers, new Coordonnees(coordX, coordY), Comparator.naturalOrder()) >= 0) {
            if (declencheException)
                throw new RuntimeException("Erreur lors de l'interprétation du fichier d'entrée : deux éléments sont dans le même case");
            else
                return true;
        }
        return false;
    }

    /**
     * Affiche la carte dans la console
     */
    public void afficherCarteDansLaConsole() {
        Comparator<Aventurier> coordComparator = (av1, av2) -> {
            if (av1.getY() < av2.getY()) return -1;
            else if (av1.getY() > av2.getY()) return 1;
            else if (av1.getX() < av2.getX()) return -1;
            else if (av1.getX() > av2.getX()) return 1;
            else return 0;
        };

        for (int y = 0; y < tailleY; y++) { // on parcours les lignes une par une
            StringBuilder ligne = new StringBuilder();

            for (int x = 0; x < tailleX; x++) { // on parcours chaque case de la ligne
                int index = Collections.binarySearch(aventuriers, new Coordonnees(x, y), Comparator.naturalOrder());
                if (index >= 0) // la case contient un anveturier
                    ligne.append(String.format("%-15s", "A(" + aventuriers.get(index).getNom() + "," + aventuriers.get(index).getNbTresorRamasses() + "," + aventuriers.get(index).getOrientation().getLabel() + ")"));
                else {
                    index = Collections.binarySearch(montagnesEtTresors, new Coordonnees(x, y), Comparator.naturalOrder());
                    if (index >= 0) { // la case contient une montagne ou un trésor
                        if (montagnesEtTresors.get(index) instanceof Tresor) {
                            ligne.append(String.format("%-15s", "T(" + ((Tresor) montagnesEtTresors.get(index)).getNbTresorsDisponibles() + ")"));
                        } else { // montagne
                            ligne.append(String.format("%-15s", "M"));
                        }
                    } else // La case contient de la plaine
                        ligne.append(String.format("%-15s", "-"));
                }
            }
            System.out.println(ligne);
        }
        System.out.println("");
    }

    /**
     * Ecrit le résultat dans un fichier texte en fin de partie
     * @param chemonCompletFichierResultat Chemin complet du fichier de sortie
     * @throws
     *      IOException : erreur lors de la lecture du fichier d'entrée ou écriture dans le fichier de sortie
     *      RuntimeException : erreur applicative
     */
    public void ecrireFichierResultat(String chemonCompletFichierResultat) throws IOException, RuntimeException {
        try {
            FileWriter writer = new FileWriter(chemonCompletFichierResultat);

            writer.write("# {C comme Carte} - {largeur} - {hauteur}\n");
            writer.write(String.format("C - %d - %d\n",tailleX, tailleY));

            writer.write("# {M comme Montagne} - {Axe horizontal} - {Axe vertical}\n");
            montagnesEtTresors.stream()
                .filter(mt -> !(mt instanceof Tresor))
                .forEach(mt -> {
                    try {
                        writer.write(String.format("M - %s - %s\n", mt.getX(), mt.getY()));
                    } catch (IOException e) {
                        throw new RuntimeException("Erreur lors de l'ecriture des montagnes dans le fichier de sortie");
                    }
                });

            writer.write("# {T comme Trésor} - {Axe horizontal} - {Axe vertical} - {Nb. de trésors restants}\n");
            montagnesEtTresors.stream()
                    .filter(tr -> tr instanceof Tresor)
                    .forEach(tr -> {
                        try {
                            writer.write(String.format("T - %s - %s - %s\n", tr.getX(), tr.getY(), ((Tresor) tr).getNbTresorsDisponibles()));
                        } catch (IOException e) {
                            throw new RuntimeException("Erreur lors de l'ecriture des trésors du fichier de sortie");
                        }
                    });

            writer.write("# {A comme Aventurier} - {Nom de l’aventurier} - {Axe horizontal} - {Axe vertical} - {Orientation} - {Nb. trésors ramassés}\n");
            aventuriers.stream()
                    .sorted(Comparator.comparing(Aventurier::getOrdre))
                    .forEach(av -> {
                        try {
                            writer.write(String.format("A - %s - %s - %s - %s - %s\n",
                                    av.getNom(), av.getX(), av.getY(), av.getOrientation().getLabel(),
                                    av.getNbTresorRamasses()));
                        } catch (IOException e) {
                            throw new RuntimeException("Erreur lors de l'ecriture des trésors du fichier de sortie");
                        }
                    });

            writer.close();
        } catch (IOException e) {
            throw new IOException("Erreur lors de l'ecriture du fichier résultat");
        }
    }

    /**
     * Joue un tour en déplacant chaque aventurier si possible
     * @param numTour numéro du tour à jouer
     * @return indique s'il reste encore des tours à jouer
     */
    public boolean jouerUnTour(int numTour) {
        boolean auMoinsUnMouvement = false;

        System.out.println("Mouvements à jouer :");
        aventuriers.stream()
            .filter(av -> av.getListeMouvements().size() > 0)
            .sorted(Comparator.comparing(Aventurier::getOrdre))
            .forEach(av -> System.out.printf("%s: %s", av.getNom(), av.getListeMouvements().getFirst()));
        System.out.println("");

        // Au début du tour, la nouvelle position n'est figée pour aucun aventurier
        aventuriers.forEach(av -> av.setNouvellePositionFigee(false));
        // Cette liste sert à parcourir les aventuriers dans l'ordre pour prendre en compte leur priorité en cas de conflit
        List<Aventurier> aventuriesDansLordre = new ArrayList<Aventurier>(aventuriers);
        aventuriesDansLordre.sort(Comparator.comparing(Aventurier::getOrdre));

        // Pour chaque tour, on réalise autant d'itérations que nécessaire sur la liste des aventuriers jusqu'à ce que ce
        // l'on sache avec certitude où il ira. Si on connait avec certitude sa nouvelle position, elle est figée
        boolean auMoinsUnPasFige = true; // permet de savoir s'il restera une itération à faire après celle-ci
        while (auMoinsUnPasFige) {
            auMoinsUnPasFige = false;
            long nbAventuriersFigesAvant = aventuriers.stream().filter(av -> av.isNouvellePositionFigee()).count();
            for (Aventurier aventurierCourant : aventuriesDansLordre) {
                if (!aventurierCourant.isNouvellePositionFigee() && aventurierCourant.listeMouvements.size() > 0) {
                    if (avancerAventurier(aventurierCourant, false))
                        auMoinsUnMouvement = true;
                    if (!aventurierCourant.isNouvellePositionFigee()) // si la nouvelle position de l'aventurier courant n'est pas figée, il y aura au moins un tour supplémentaire
                        auMoinsUnPasFige = true;
                }
            }
            long nbAventuriersFigesApres = aventuriers.stream().filter(av -> av.isNouvellePositionFigee()).count();
            if (nbAventuriersFigesApres == nbAventuriersFigesAvant) {
                // on est dans une boucle infinie où une partie des aventuriers se bloquent mutuellement
                aventuriers.stream().filter(av -> !av.isNouvellePositionFigee())
                        .forEach(av -> avancerAventurier(av, true));
            }
        }
        // A la fin du tour, on retire le premier mouvement de chaque aventurier et on indique s'il reste encore des tours à jouer
        aventuriers.forEach(av -> av.getListeMouvements().pollFirst());

        if (auMoinsUnMouvement) {
            System.out.println("Carte après le tour " + numTour + " :");
            afficherCarteDansLaConsole();
        } else {
            System.out.println("\nCarte après le tour " + numTour + " : aucun aventurier n'a changé de place\n");
        }

        return aventuriers.stream()
                .anyMatch(av -> !av.listeMouvements.isEmpty());
    }

    /**
     * Tenter de déplace un aventurier
     *
     * @param aventurier
     *      L'objet aventurier contenant notemment sont prochain déplacement et son orientation
     * @return true si l'aventurier a effectivement avancé
     */
    private boolean avancerAventurier(Aventurier aventurier, boolean forcerDebloquage) {
        boolean aventurierAvance = false;

        Mouvement mouvement = aventurier.listeMouvements.peekFirst();
        if (mouvement != null) {
            switch (mouvement) {
                case TOURNER_A_DROITE:
                    System.out.printf("%s tourne à droite", aventurier.getNom());
                    switch (aventurier.getOrientation()) {
                        case NORD: aventurier.setOrientation(Orientation.EST); break;
                        case EST: aventurier.setOrientation(Orientation.SUD); break;
                        case SUD: aventurier.setOrientation(Orientation.OUEST); break;
                        default: aventurier.setOrientation(Orientation.NORD); break;
                    }
                    aventurier.setNouvellePositionFigee(true);
                    break;
                case TOURNER_A_GAUCHE:
                    System.out.printf("%s tourne à gauche", aventurier.getNom());
                    switch (aventurier.getOrientation()) {
                        case NORD: aventurier.setOrientation(Orientation.OUEST); break;
                        case EST: aventurier.setOrientation(Orientation.NORD); break;
                        case SUD: aventurier.setOrientation(Orientation.EST); break;
                        default: aventurier.setOrientation(Orientation.SUD); break;
                    }
                    aventurier.setNouvellePositionFigee(true);
                    break;
                case AVANCER:
                    Coordonnees prochaineCase = new Coordonnees(aventurier.getX(), aventurier.getY());
                    boolean avancerAventurier = false;
                    switch (aventurier.getOrientation()) {
                        case NORD: prochaineCase.setY(prochaineCase.getY() - 1); break;
                        case EST: prochaineCase.setX(prochaineCase.getX() + 1); break;
                        case SUD: prochaineCase.setY(prochaineCase.getY() + 1); break;
                        default: prochaineCase.setX(prochaineCase.getX() - 1); break;
                    }
                    if (prochaineCase.getX() >= 0 && prochaineCase.getY() >= 0 && prochaineCase.getX() < tailleX && prochaineCase.getY() < tailleY) {
                        // la prochaine case est bien dans la carte
                        if (forcerDebloquage) {
                            // des aventuriers sont bloqués dans une boucle. On les fait tous avancer
                            System.out.printf("%s est bloqué dans une boucle. On le fait avancer", aventurier.getNom());
                            avancerAventurier = true; // notre aventurier avance et sa nouvelle position est figée
                            aventurier.setNouvellePositionFigee(true);
                        } else {
                            // On teste si elle contient un aventurier
                            Optional<Aventurier> avProchaineCase = aventuriers.stream()
                                    .filter(av -> Objects.equals(av.getX(), prochaineCase.getX()) && Objects.equals(av.getY(), prochaineCase.getY()))
                                    .findFirst();
                            if (avProchaineCase.isPresent()) { // la case contient un aventurier
                                if (avProchaineCase.get().isNouvellePositionFigee()) {
                                    // l'aventurier de la prochaine case est figé
                                    System.out.printf("%s est bloqué par %s",
                                            aventurier.getNom(), avProchaineCase.get().getNom());
                                    aventurier.setNouvellePositionFigee(true); // notre aventurier n'avance pas et sa nouvelle position est figée
                                } else { // l'aventurier de la prochaine case n'est pas figé, on ne fait rien. On attends le prochain tour
                                    System.out.printf("%s est peut être bloqué par %s en %s-%s. On attends son mouvement",
                                            aventurier.getNom(), avProchaineCase.get().getNom(), prochaineCase.getX(), prochaineCase.getY());
                                }
                            } else { // On cherche si la prochaine case contient une montagne ou un trésor
                                int iMetT = Collections.binarySearch(montagnesEtTresors, prochaineCase, Comparator.naturalOrder());
                                if (iMetT >= 0) { // s'il y a une montagne ou un trésor dans la prochaine case
                                    if (montagnesEtTresors.get(iMetT) instanceof Tresor) { // c'est un trésor
                                        Tresor tresor = (Tresor) montagnesEtTresors.get(iMetT);
                                        if (tresor.getNbTresorsDisponibles() > 0) {
                                            System.out.printf("%s avance en %s-%s et trouve un trésor",
                                                    aventurier.getNom(), prochaineCase.getX(), prochaineCase.getY());
                                            aventurier.setNbTresorRamasses(aventurier.getNbTresorRamasses() + 1);
                                            tresor.setNbTresorsDisponibles(tresor.getNbTresorsDisponibles() - 1);
                                        } else {
                                            System.out.printf("%s avance en %s-%s, sur une case trésor vide",
                                                    aventurier.getNom(), prochaineCase.getX(), prochaineCase.getY());
                                        }
                                        avancerAventurier = true;
                                        aventurier.setNouvellePositionFigee(true);
                                    } else { // c'est une montagne. On ne déplace pas l'aventurier. Sa nouvelle position est donc figée
                                        System.out.printf("%s est bloqué par une montagne en %s-%s",
                                                aventurier.getNom(), prochaineCase.getX(), prochaineCase.getY());
                                        aventurier.setNouvellePositionFigee(true);
                                    }
                                } else { // il y a de la plaine
                                    System.out.printf("%s avance en %s-%s, sur une case libre",
                                            aventurier.getNom(), prochaineCase.getX(), prochaineCase.getY());
                                    avancerAventurier = true;
                                }
                            }
                        }
                    } else {// la prochaine position est hors de la carte : l'aventurier n'avance pas et sa position est figée
                        System.out.printf("%s ne peut pas sortir de la carte et reste en %s-%s",
                                aventurier.getNom(), aventurier.getX(), aventurier.getY());
                        aventurier.setNouvellePositionFigee(true);
                    }

                    if (avancerAventurier) {
                        aventurier.setX(prochaineCase.getX()); // on déplace l'aventurier
                        aventurier.setY(prochaineCase.getY());
                        aventurier.setNouvellePositionFigee(true);
                        // Lorsqu'un aventurier avance, la liste des aventuriers doit être triée à nouveau pour rester dans l'ordre
                        if (!forcerDebloquage)
                            Collections.sort(aventuriers);
                        aventurierAvance = true;
                    }
            }
        } else {
            throw new RuntimeException("Un mouvement ne peut pas être null");
        }
        return aventurierAvance;
    }
}