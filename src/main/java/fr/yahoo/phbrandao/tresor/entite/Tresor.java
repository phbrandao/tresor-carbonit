package fr.yahoo.phbrandao.tresor.entite;

import lombok.Getter;
import lombok.Setter;

/**
 * Un trésor est défini par ses coordonnées sur la carte et le nombre de trésors disponibles
 */
@Getter
@Setter
public final class Tresor extends Coordonnees {
    // Nombre de trésors aux coordonnées indiquées
    private int nbTresorsDisponibles;

    /**
     * Constructeur
     * @param x coordonnées en abcisse du trésor
     * @param y coordonnées en ordonnée du trésor
     * @param nbTresorsDisponibles Nombre de trésors disponibles à la coordonnée indiquée
     */
    public Tresor(int x, int y, int nbTresorsDisponibles) {
        super(x, y);
        this.nbTresorsDisponibles = nbTresorsDisponibles;
    }
}
