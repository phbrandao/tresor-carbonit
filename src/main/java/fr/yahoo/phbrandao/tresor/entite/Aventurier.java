package fr.yahoo.phbrandao.tresor.entite;

import fr.yahoo.phbrandao.tresor.enumerations.Mouvement;
import fr.yahoo.phbrandao.tresor.enumerations.Orientation;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayDeque;
import java.util.Comparator;
import java.util.Deque;

/**
 * Un aventurier est défini par son ordre, non, orientation, le nombre de trésors ramassés
 * et ses coordonnées sur la carte
 */
@Getter
@Setter
public final class Aventurier extends Coordonnees {
    // Ordre d'apparition de l'aventurier dans le fichier d'entrée
    private Integer ordre;

    private String nom;

    // Nombre de trésors ramassés par l'aventurier tout au long de son parcours
    private int nbTresorRamasses = 0;

    // Orientation courante de l'aventurier (nord, sud,...)
    private Orientation orientation;

    Deque<Mouvement> listeMouvements;

    private boolean nouvellePositionFigee;

    /**
     * Constructeur d'un aventurier
     * @param x Coordonnées X
     * @param y Coordonnées Y
     * @param ordre Ordre de priorité utilisée lors des mouvements de l'aventurier
     * @param nom Nom de l'aventurier
     * @param orientation Orientation utilisée pour le prochain mouvement
     * @param listeMouvements Liste des prochains mouvements
     */
    public Aventurier(int x, int y, int ordre, String nom, Orientation orientation, Deque<Mouvement> listeMouvements) {
        super(x, y);
        this.ordre = ordre;
        this.nom = nom;
        this.orientation = orientation;
        this.listeMouvements = listeMouvements;
    }
}
