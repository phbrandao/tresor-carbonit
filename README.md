# Chasse au trésor au pérou

Cette application fait évoluer des aventuriers sur une carte au cours d'une chasse aux trésors. Les spécifications complètes sont dans un document PDF, et ne sont pas rappelées ici.

La documentation technique générée au format Javadoc est disponible dans le répertoire javadoc à la racine du projet.

J'ai fait les choix suivants, qui n'étaient pas précisés dans l'énoncé :

- au démarage d'un tour, tous les aventuriers bougent en même temps. Cela signifie par exemple que 3 aventurier peuvent se suivre en file indienne sans se géner mutuellement,
- pour ramasser un trésor, un aventurier se place sur la case du trésor, A l'affichage de la carte, seul l'aventurier est alors visible sur cette case. Le trésor ne réapparait dans la console ou le fichier de sortie que lorsque l'aventurier quitte la case,
- deux aventuriers avançant face à face permuttent leur position sans se gêner

## Implémentation technique

Le projet est écrit en Java 21, et utilise Maven. Il a été initialisé avec l'archetype Maven "Quickstart".

### Comment l'exécuter depuis un IDE ?
Ouvrir le projet Git dans un IDE (Intellij IDEA, Eclipse, VS code,...) configuré avec Maven 3.x et un JDK 21.  
Executer la méthode main de la classe ChasseAuTresor en mode Run ou Debug.  
Si le programme est exécuter sans paramètres, il utilise par défaut un fichier d'entrée de test, et produit en sortie le fichier resultat.txt à la racine du projet.

### Comment l'exécuter en ligne de commande
Pré-requis : avoir installé en local Maven 3.x et un JDK 21.

Ouvrir un terminal à la racine du projet Git cloné en local, et lancer la commande suivante pour packager le programme dans un fichier jar :
```
mvn package
```

Puis exécuter le programme en spécifiant en paramètres les chemins complets des fichiers d'entrée et sortie :
```
cd target
java -cp tresor-carbonit-1.0-SNAPSHOT.jar fr.yahoo.phbrandao.tresor.ChasseAuxTresor ../src/test/resources/carte/10_scenario_complet.txt resultat.txt
```